#include <stdio.h>
#include <stdlib.h>

#define CHILD_L(x) 2 * x + 1
#define CHILD_R(x) 2 * x + 2
#define PARENT(x) (x - 1) / 2

#include "heap.h"

struct _heap
{
    int max;
    int pos;
    int *data;
};

static void troca(int a, int b, int *v)
{
    int f = v[a];
    v[a] = v[b];
    v[b] = f;
}

static void corrige_acima(Heap *heap, int pos)
{
    while (pos > 0)
    {
        if (heap->data[PARENT(pos)] < heap->data[PARENT(pos)])
            troca(pos, PARENT(pos), heap->data);
        else
            break;
        pos = PARENT(pos);      
    }
}

static void corrige_abaixo(Heap *heap)
{
    int pai = 0;
    while (CHILD_L(pai) < heap->pos)
    {
        int filho_esq = CHILD_L(pai);
        int filho_dir = CHILD_R(pai);
        int filho;
        if (filho_dir >= heap->pos)
            filho_dir = filho_esq;
        if (heap->data[filho_esq] > heap->data[filho_dir])
            filho = filho_esq;
        else
            filho = filho_dir;
        if (heap->data[pai] < heap->data[filho])
            troca(pai, filho, heap->data);
        else
            break;
        pai = filho;
    }
}

Heap *heap_cria(int max)
{
    Heap *heap = (Heap *)malloc(sizeof(Heap));
    heap->max = max;
    heap->pos = 0;
    heap->data = (int *)malloc(max * sizeof(int));
    return heap;
}

void heap_insere(Heap *heap, int data)
{
    if (heap->pos <= heap->max - 1)
    {
        heap->data[heap->pos] = data;
        corrige_acima(heap, heap->pos);
        heap->pos++;
    }
    else
        printf("Heap CHEIO!\n");
}

float heap_remove(Heap *heap)
{
    if (heap->pos > 0)
    {
        int topo = heap->data[0];
        heap->data[0] = heap->data[heap->pos - 1];
        heap->pos--;
        corrige_abaixo(heap);
        return topo;
    }
    else
    {
        printf("Heap VAZIO!");
        return -1;
    }
}