#ifndef HEAP_H_
#define HEAP_H_

typedef struct _heap Heap;

Heap* heap_cria(int max);

void heap_insere(Heap* heap, int data);

float heap_remove(Heap* heap);

void heap_print(Heap *heap);

void heap_print_indent(Heap *heap);

#endif // HEAP_H_