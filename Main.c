#include <stdio.h>
#include <stdlib.h>

#include "menu.h"
#include "heap.h"

int readFromKeyBoard()
{
	int value;
	int ch;
	scanf(" %d", &value);
	while ((ch = getchar()) != '\n')
		;
	return value;
}

int main(void)
{
	
	int loopFlag = 1;
	int value;

	Heap *heap = NULL;

	while (loopFlag)
	{
		int opt = getMenuOption("Criar a estrutura de dados (Construção da Heap);",
								"Inserir um elemento;",
								"Recuperar/Buscar um determinado elemento;",
								"Remover um determinado elemento;",
								"Alterar o valor de um determinado elemento;",
								"Liberar a estrutura de dados;,",
								NULL);
		printf("\nopt chose ----> %d\n", opt);

		switch (opt)
		{
			case 1:
				printf("\nDigite o tamanho maximo:\n");
				value = readFromKeyBoard();
				heap_cria(value);
				break;
			case 2:
				if (heap == NULL) {
					printf("\nheap vazio\n");
					continue;
				}
				printf("\nDigite valor a ser inserido:\n");
				value = readFromKeyBoard();
				heap_insere(heap, value);
				break;
			case 3:
				if (heap == NULL) {
					printf("\nheap vazio\n");
					continue;
				}

				break;
			case 4:
				if (heap == NULL) {
					printf("\nheap vazio\n");
					continue;
				}
				break;
			case 5:
				if (heap == NULL) {
					printf("\nheap vazio\n");
					continue;
				}
				break;
			case 6:
				if (heap == NULL) {
					printf("\nheap vazio\n");
					continue;
				}	
				
				break;
		default:
			loopFlag = 0;
			break;
		}
	}
	getchar();
	return 0;
}